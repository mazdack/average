package com.mazdack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Average {
  public static void main(String[] args) throws IOException {
    Average average = new Average();
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    while(true) {
      System.out.format("Please enter int between %s and %s:%n", Integer.MIN_VALUE, Integer.MAX_VALUE);
      String name = reader.readLine();
      Integer value;
      try {
        value = Integer.valueOf(name);
      } catch (NumberFormatException e) {
        System.out.format("Number should be between %s and %s...%n", Integer.MIN_VALUE, Integer.MAX_VALUE);
        continue;
      }
      average.input(value);
      System.out.format("Min = %s, Max = %s, Average = %s%n", average.getMin(), average.getMax(), average.getAverage());
    }
  }

  private Integer currentMin;
  private Integer currentMax;

  private double sum;
  private int numberOfValues;

  public void input(int value) {

    if (currentMin == null) {
      currentMin = value;
    } else {
      currentMin = Math.min(currentMin, value);
    }

    if (currentMax == null) {
      currentMax = value;
    } else {
      currentMax = Math.max(currentMax, value);
    }

    sum += value;
    numberOfValues++;
  }


  public int getMin() {
    return currentMin;
  }

  public int getMax() {
    return currentMax;
  }

  public double getAverage() {
    return sum / numberOfValues;
  }
}
