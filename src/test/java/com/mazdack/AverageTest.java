package com.mazdack;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.Objects;
import java.util.stream.Stream;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class AverageTest {

  private Average average = new Average();

  @ParameterizedTest
  @MethodSource("getInput")
  void shouldCalculateCorrectStatistics(int[] values) {
    Integer[] encounteredValues = new Integer[values.length];

    for (int i = 0; i < values.length; i++) {
      int value = values[i];
      encounteredValues[i] = value;

      IntSummaryStatistics summary = Arrays.stream(encounteredValues)
        .filter(Objects::nonNull)
        .mapToInt(Integer::intValue)
        .summaryStatistics();

      average.input(value);

      assertEquals(summary.getMin(), average.getMin(), String.format("Error while getting min %s", Arrays.toString(encounteredValues)));
      assertEquals(summary.getMax(), average.getMax(), String.format("Error while getting max %s", Arrays.toString(encounteredValues)));
      assertEquals(summary.getAverage(), average.getAverage(), String.format("Error while getting average %s", Arrays.toString(encounteredValues)));

    }
  }

  static Stream<Arguments> getInput() {
    return Stream.of(
      Arguments.arguments(new int[]{5, 6, 7, 1, 1, 2, 1000}),
      Arguments.arguments(new int[]{Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE}),
      Arguments.arguments(new int[]{Integer.MAX_VALUE, Integer.MAX_VALUE}),
      Arguments.arguments(new int[]{0, 0, 0, 0, Integer.MAX_VALUE}),
      Arguments.arguments(new int[]{0, 0, 0, 0, Integer.MIN_VALUE})
    );
  }
}
